document.addEventListener("DOMContentLoaded", function () {
  const buttons = document.getElementById("load-more");
  if (buttons) {
    buttons.addEventListener("click", handler);
  }

  function handler() {
    const newArticle = [
      {
        title: "Cara Membuat Kopi",
        desc: "Kopi hitam merupakan salah satu menu kopi esensial yang sering disajikan baik di rumah maupun di kafe. Pada tiap cangkir kopi hitam, Anda akan dapat merasakan cita rasa kopi seutuhnya tanpa adanya bahan-bahan lainnya, seperti susu dan juga cokelat.",
        url: "https://www.dolce-gusto.co.id/cara-membuat-kopi-hitam",
        img: "./asset/kopi.jpeg",
      },
      {
        title: "Manfaat Kopi Hitam",
        desc: "Kopi hitam menurut sebagian orang merupakan satu di antara minuman terbaik di dunia. Minuman ini diklaim memiliki beragam manfaat kesehatan bagi sejumlah orang. Manfaat kopi bagi kesehatan bisa didapat dengan meminumnya secara polos atau tanpa gula. Kopi hitam tanpa gula akan memberikan sensasi tersendiri bagi yang meminumnya dan lebih sehat.",
        url: "https://www.bola.com/ragam/read/4473159/13-manfaat-kopi-hitam-bagi-kesehatan-tak-hanya-menghilangkan-kantuk",
        img: "./asset/kopi-hitam.jpg",
      },
    ];

    let elemNewArticle = "";

    for (article of newArticle) {
      elemNewArticle += `<article class="box"><h2>${article.title}</h2> <img class="img-article" src="${article.img}"><p>${article.desc}</p><a rel="nofollow" target="_blank" href="${article.url}">Selengkapnya</a><div class="clear"></div></article>`;
    }

    const elemListArticle = document
      .getElementById("load-more")
      .closest(".center");
    elemListArticle.insertAdjacentHTML("beforebegin", elemNewArticle);
    buttons.remove();
  }
});
